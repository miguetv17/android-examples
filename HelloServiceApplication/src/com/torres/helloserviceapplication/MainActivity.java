package com.torres.helloserviceapplication;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/*
		 * getMenuInflater() is a method of the Activity class that gives you a
		 * MenuInflater. MenuInflater is used to inflate menu xml files(defined
		 * in your /res/menu folder) and gives you Menu object. With the Menu
		 * object in your hand you can play around with it and do whatever you
		 * want.
		 */
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// Method to start the service
	public void startService(View view){
		startService(new Intent(getBaseContext(), MyService.class));
	}
	
	// Method to stop the service
	public void stopService(View view){
		stopService(new Intent(getBaseContext(), MyService.class));
	}
	
}
